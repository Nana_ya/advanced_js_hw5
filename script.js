let requestUsers = 'https://ajax.test-danit.com/api/json/users';
let requestPosts = 'https://ajax.test-danit.com/api/json/posts';

function createCard() {
    fetch(requestUsers).then(response => response.json())
        .then(resultName => {
            fetch(requestPosts).then(response => response.json())
                .then(resultPost => {
                    resultName.forEach(({name, email, id}) => {
                        resultPost.forEach(({title, body, userId, id: postID}) => {
                            if (id === userId) {
                                new Card(name, email, title, body, postID).render();
                            }
                        })
                    })
                })

        })
}

class Card {
    constructor(name, email, title, body, id) {
        this.title = title;
        this.body = body;
        this.name = name;
        this.email = email;
        this.id = id;
    }

    render() {
        const card = document.createElement('div');
        card.classList.add('card');

        card.innerHTML += 
        `<p class="title">${this.title}</p>
        <p class="text">${this.body}</p>
        <span class="name">${this.name}</span>
        <span class="email">Email: ${this.email}</span> `;
        const button = document.createElement('button');
        button.innerText = "Delete";
        
        card.append(button);
        cardContainer.append(card);

        button.addEventListener('click', (event) => {
            event.preventDefault()
            fetch(requestPosts + '/' + this.id, {
                method: 'DELETE',
            }).then(() => {button.closest('.card').remove()})
        })
    }
}

const cardContainer = document.createElement('div');
cardContainer.classList.add('card-container');
document.body.append(cardContainer);
createCard();
